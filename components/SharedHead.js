import Head from 'next/head'
import { useContext } from 'react'
import { AppContext } from '../pages/_app'

export default function SharedHead({ title }) {
  const { config } = useContext(AppContext)

  return (
    <Head>
      <title>{`${config?.appName} - ${title}`}</title>
    </Head>
  )
}