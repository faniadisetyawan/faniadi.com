import { useContext, useState } from 'react'
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavbarText, Container } from 'reactstrap'
import { AppContext } from '../../pages/_app'
import Link from 'next/link'

export default function SharedNavbar() {
  const { config } = useContext(AppContext)

  const [isOpen, setIsOpen] = useState(false)

  const toggle = () => setIsOpen(!isOpen)

  return (
    <Navbar color="white" light expand="md" className="border-bottom">
      <Container>
        <NavbarBrand tag={Link} href="/">
          <a className="navbar-brand">
            <b>{config?.appName}</b>
          </a>
        </NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <Link href="/">
                <a className="nav-link">Home</a>
              </Link>
            </NavItem>
            <NavItem>
              <Link href="/projects">
                <a className="nav-link">Projects</a>
              </Link>
            </NavItem>
          </Nav>
          <NavbarText className="ms-auto">{config?.appTagline}</NavbarText>
        </Collapse>
      </Container>
    </Navbar>
  )
}