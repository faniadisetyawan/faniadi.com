import SharedFooter from './SharedFooter'
import SharedNavbar from './SharedNavbar'

export default function SharedLayout({ children }) {
  return (
    <>
      <SharedNavbar/>

      {children}

      <SharedFooter/>
    </>
  )
}