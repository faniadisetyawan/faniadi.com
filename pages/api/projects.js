export default (req, res) => {
  res.status(200).json([
    {
      id: 1,
      title: 'Project 01',
      slug: 'project-01',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
      cover: 'https://picsum.photos/id/237/400/250'
    },
    {
      id: 2,
      title: 'Project 02',
      slug: 'project-02',
      description: 'Nunc sed blandit libero volutpat sed cras ornare arcu. Proin nibh nisl condimentum id venenatis a condimentum.',
      cover: 'https://picsum.photos/id/1011/400/250'
    }
  ])
}