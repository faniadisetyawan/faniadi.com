export default (req, res) => {
  res.status(200).json({
    appName: "FaniAdi",
    appTagline: "Freelance Web Developer",
    appDescription: "Hi, my name is Fani Adi Setyawan. I'm a Freelance Web Developer"
  })
}