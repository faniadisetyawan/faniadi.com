import { useContext } from 'react'
import SharedHead from '../components/SharedHead'
import { AppContext } from './_app'

export default function Home() {
  const { config } = useContext(AppContext)

  return (
    <>
      <SharedHead title={config?.appDescription} />

      <div className="cover">
        <p className="lead">Homepage</p>
      </div>
    </>
  )
}
