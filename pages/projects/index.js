import { Card, CardBody, CardImg, CardText, CardTitle, Col, Container, Row } from 'reactstrap'
import SharedHead from '../../components/SharedHead'
import api from '../../services/api'

export default function Projects({ data }) {
  return (
    <>
      <SharedHead title={`My Personal Projects`} />

      <div className="content-wrapper">
        <Container>
          <h3>My Personal Projects</h3>

          <Row>
            {data?.map((item, i) => (
              <Col key={i} sm={4} className="d-flex justify-content-stretch">
                <Card className="my-3">
                  <CardImg top width="100%" src={item?.cover} />
                  <CardBody>
                    <CardTitle>{item?.title}</CardTitle>
                    <CardText>{item?.description}</CardText>
                  </CardBody>
                </Card>
              </Col>
            ))}
          </Row>
        </Container>
      </div>
    </>
  )
}

export async function getServerSideProps() {
  const { data } = await api.get('/projects')

  return {
    props: { data }, // will be passed to the page component as props
  }
}