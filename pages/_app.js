import { createContext, useEffect, useState } from 'react'
import SharedLayout from '../components/layouts/SharedLayout'
import SharedLoading from '../components/SharedLoading'
import api from '../services/api'
import '../styles/app.scss'

export const AppContext = createContext({
  config: {}
})

function MyApp({ Component, pageProps }) {
  const [loading, setLoading] = useState(true)
  const [config, setConfig] = useState({})

  const fetchConfig = async () => {
    setLoading(true)
    try {
      const { data } = await api.get('/config')
      setConfig(data)
      setLoading(false)
    } catch (err) {
      setLoading(false)
    }
  }

  useEffect(() => {
    fetchConfig()
  }, [])

  if (loading) {
    return <SharedLoading/>
  }

  return (
    <AppContext.Provider
      value={{
        config: config
      }}
    >
      <SharedLayout>
        <Component {...pageProps} />
      </SharedLayout>
    </AppContext.Provider>
  )
}

export default MyApp
