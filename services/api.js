import Axios from 'axios'

export const baseURL = process.env.NEXT_PUBLIC_API_URL

const api = Axios.create({
  baseURL: baseURL,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  }
})

export default api